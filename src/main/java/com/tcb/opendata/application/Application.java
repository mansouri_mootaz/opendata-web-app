package com.tcb.opendata.application;


import com.tcb.opendata.configuration.*;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by mootaz
 */
@Configuration
@ComponentScan(basePackages = "com.tcb.opendata")
@EnableAutoConfiguration(exclude = {ElasticsearchConfiguration.class})
public class Application {
    public static void main(String args[]){
        SpringApplication.run(Application.class);
    }
}


//webjars/bootstrap/3.3.6/