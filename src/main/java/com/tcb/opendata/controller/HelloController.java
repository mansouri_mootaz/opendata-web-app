package com.tcb.opendata.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.tcb.opendata.entities.Ckan_metadata;
import com.tcb.opendata.service.Ckan_metadataService;

@Controller
public class HelloController {

	@Autowired
	private Ckan_metadataService ckan_metadataService;

	@RequestMapping(value = "/hello", params = { "q" })
	public String sayHello(@RequestParam("q") String keywords,
			@RequestParam(value = "p", required = false, defaultValue = "1") int page, ModelMap model) {

		Page<Ckan_metadata> entries = ckan_metadataService.findbytagsnamewithscore(keywords,
				new PageRequest((page - 1) * 10, page * 10));

		List<Ckan_metadata> entrieList = new ArrayList<Ckan_metadata>();
		for (Iterator<Ckan_metadata> iterator = entries.iterator(); iterator.hasNext();) {
			entrieList.add(iterator.next());
		}
		model.put("entries", entrieList);
		return "hello";
	}

	@RequestMapping(value = "")
	public String sayHello() {
		return "search-page"; //the name of the ftl file
	}

	@RequestMapping(value = "/essai")
	public String sayHelloa() {
		return "essai"; //the name of the ftl file
	}


}
