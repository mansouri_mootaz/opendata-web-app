package com.tcb.opendata.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import com.tcb.opendata.entities.Ckan_metadata;

/**
 * Created by mootaz on 07/08/2016.
 */
public interface Ckan_metadataService {

	Ckan_metadata save(Ckan_metadata ckan_metadata);

	Ckan_metadata findOne(String name);

	Iterable<Ckan_metadata> findAll();

	Page<Ckan_metadata> findByTagsName(String tagName, PageRequest pageRequest);
	Page<Ckan_metadata>  findbytagsnamewithscore(String tagname,Pageable pageRequest);

	
}