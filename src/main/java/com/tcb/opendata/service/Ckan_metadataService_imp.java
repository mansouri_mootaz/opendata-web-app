package com.tcb.opendata.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.tcb.opendata.entities.Ckan_metadata;
import com.tcb.opendata.repository.Ckan_metadataRepository;

@Service
public class Ckan_metadataService_imp implements Ckan_metadataService {

	@Autowired
	private Ckan_metadataRepository ckan_metadataRepository;

	@Override
	public Ckan_metadata save(Ckan_metadata ckan_metadata) {
		return ckan_metadataRepository.save(ckan_metadata);
	}

	@Override
	public Ckan_metadata findOne(String id) {
		return ckan_metadataRepository.findOne(id);

	}

	@Override
	public Iterable<Ckan_metadata> findAll() {
		return ckan_metadataRepository.findAll();
	}

	@Override
	public Page<Ckan_metadata> findByTagsName(String tagName, PageRequest pageRequest) {
		return ckan_metadataRepository.findByTagsName(tagName, pageRequest);
	}

	@Override 
	public Page<Ckan_metadata>   findbytagsnamewithscore(String Tagname,Pageable pageRequest){
		return ckan_metadataRepository. findbytagsnamewithscore(Tagname,pageRequest);
		
	}

}
