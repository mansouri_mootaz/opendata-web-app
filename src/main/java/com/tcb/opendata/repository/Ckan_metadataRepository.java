package com.tcb.opendata.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.annotations.Query;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import com.tcb.opendata.entities.Ckan_metadata;

/**
 * @author mootaz
 */
public interface Ckan_metadataRepository extends ElasticsearchRepository<Ckan_metadata, String> {

	Page<Ckan_metadata> findByTagsName(String name, Pageable pageable);
	
	@Query("{\"match\": {\"tags.name\": \"?0\"}}")
	Page<Ckan_metadata> findbytagsnamewithscore(String name, Pageable pageRequest);
}