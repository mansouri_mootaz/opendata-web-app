package com.tcb.opendata.entities;

public class Resource {
	String resource_group_id;
	String format;
	String resource_type;
	String url;

	Tracking_summary tracking_summary;
	String description;
	String language;
	java.util.Date cache_last_updated;
	String revision_timestamp;
	String id;
	String state;
	java.util.Date last_modified;
	String mimetype_inner;
	String revision_id;
	String characterset;
	String url_type;
	java.util.Date created;
	String size;
	String cache_url;
	java.util.Date webstore_last_updated;
	String webstore_url;
	String name;
	String mimetype;
	String position;
	String hash;

	public Tracking_summary getTracking_summary() {
		return tracking_summary;
	}

	public void setTracking_summary(Tracking_summary tracking_summary) {
		this.tracking_summary = tracking_summary;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getRevision_timestamp() {
		return revision_timestamp;
	}

	public void setRevision_timestamp(String revision_timestamp) {
		this.revision_timestamp = revision_timestamp;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getMimetype_inner() {
		return mimetype_inner;
	}

	public void setMimetype_inner(String mimetype_inner) {
		this.mimetype_inner = mimetype_inner;
	}

	public String getRevision_id() {
		return revision_id;
	}

	public void setRevision_id(String revision_id) {
		this.revision_id = revision_id;
	}

	public String getCharacterset() {
		return characterset;
	}

	public void setCharacterset(String characterset) {
		this.characterset = characterset;
	}

	public String getUrl_type() {
		return url_type;
	}

	public void setUrl_type(String url_type) {
		this.url_type = url_type;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getCache_url() {
		return cache_url;
	}

	public void setCache_url(String cache_url) {
		this.cache_url = cache_url;
	}

	public java.util.Date getCache_last_updated() {
		return cache_last_updated;
	}

	public void setCache_last_updated(java.util.Date cache_last_updated) {
		this.cache_last_updated = cache_last_updated;
	}

	public java.util.Date getLast_modified() {
		return last_modified;
	}

	public void setLast_modified(java.util.Date last_modified) {
		this.last_modified = last_modified;
	}

	public java.util.Date getCreated() {
		return created;
	}

	public void setCreated(java.util.Date created) {
		this.created = created;
	}

	public java.util.Date getWebstore_last_updated() {
		return webstore_last_updated;
	}

	public void setWebstore_last_updated(java.util.Date webstore_last_updated) {
		this.webstore_last_updated = webstore_last_updated;
	}

	public String getWebstore_url() {
		return webstore_url;
	}

	public void setWebstore_url(String webstore_url) {
		this.webstore_url = webstore_url;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMimetype() {
		return mimetype;
	}

	public void setMimetype(String mimetype) {
		this.mimetype = mimetype;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	public String getResource_group_id() {
		return resource_group_id;
	}

	public void setResource_group_id(String resource_group_id) {
		this.resource_group_id = resource_group_id;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public String getResource_type() {
		return resource_type;
	}

	public void setResource_type(String resource_type) {
		this.resource_type = resource_type;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}
