package com.tcb.opendata.entities;

public class Tracking_summary {
	long total;
	long recent;
	public long getTotal() {
		return total;
	}
	public void setTotal(long total) {
		this.total = total;
	}
	public long getRecent() {
		return recent;
	}
	public void setRecent(long recent) {
		this.recent = recent;
	}

}
