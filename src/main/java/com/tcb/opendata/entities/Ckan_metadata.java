package com.tcb.opendata.entities;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

/**
 * @author mootaz
 */
@Document(indexName = "ckan_clean_fields", type = "Spark", shards = 1, replicas = 0)
public class Ckan_metadata {

	@Id
	String name;
	String author;
	String title;
	String url;
	@Field(type = FieldType.Object) // (Nested) lets us define Tag in a separate
									// class and solve a dependancy between fields
	List<Tag> tags;
	String isopen;
	String notes;
	@Field(type = FieldType.Object)
	
	List<Resource> resources ; 

	public List<Resource> getResources() {
		return resources;
	}

	public void setResources(List<Resource> resources) {
		this.resources = resources;
	}

	public Ckan_metadata(String name, String author, String title, String url) {
		this.name = name;
		this.author = author;
		this.title = title;
		this.url = url;
	}

	public String getIsopen() {
		return isopen;
	}

	public void setIsopen(String isopen) {
		this.isopen = isopen;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Ckan_metadata() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public List<Tag> getTags() {
		return tags;
	}

	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}

	@Override
	public String toString() {
		return "ckan_metadata {" + getName() + getAuthor() + getAuthor() + getUrl() + "}";
	}
}