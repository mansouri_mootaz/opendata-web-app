package com.tcb.opendata.entities;

public class Tag {
    String name ; 
    String display_name ; 
    String vocabulary_id;
    String revision_timestamp;
    String state;
    String id;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDisplay_name() {
		return display_name;
	}
	public void setDisplay_name(String display_name) {
		this.display_name = display_name;
	}
	public String getVocabulary_id() {
		return vocabulary_id;
	}
	public void setVocabulary_id(String vocabulary_id) {
		this.vocabulary_id = vocabulary_id;
	}
	public String getRevision_timestamp() {
		return revision_timestamp;
	}
	public void setRevision_timestamp(String revision_timestamp) {
		this.revision_timestamp = revision_timestamp;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	@Override
	public String toString(){
		return "Tag {"+getDisplay_name()+"}";
	}
    
}
