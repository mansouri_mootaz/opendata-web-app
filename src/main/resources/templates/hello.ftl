<!DOCTYPE html>

<html lang="en">
<head>

    <link href="webjars/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
       <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Dashboard Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="webjars/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="dashboard.css" rel="stylesheet">


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    
 </head>   
<body>
 
	<#if entries??>
	 <#list entries as entrie>
		<div>
	        <h3>${entrie.title} </h3>
	  	 <#if entrie.resources??>
	   		<#list entrie.resources as resources >     
	        	<dl class="dl-horizontal">

	        <dt>${resources.format!"null"}</dt> <dd><a href="${resources.url!"null"}">  ${resources.url!"null"} </a></dd>
	        	</dl>
	    	</#list> 
	 	 </#if>    	
	    </div>
	  	</#list>
	</#if>
   <!--webjars/jquery-ui/1.9.2
   webjars/jquery/1.11.1-->
	 <!-- Bootstrap core JavaScript
    ================================================== -->
    
    <script src="webjars/jquery/1.11.1/jquery.min.js"></script>
        <script src="webjars/jquery/1.11.1/jquery.min.js"></script>
                <script src="webjars/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        
    
    
    
   <!-- <script>window.jQuery || document.write('<script src="webjars/jquery/1.11.1
/jquery.min.js"><\/script>')</script><script src="webjars/bootstrap/3.3.6/js/bootstrap.min.js"></script>-->

   
	
</body>

</html>